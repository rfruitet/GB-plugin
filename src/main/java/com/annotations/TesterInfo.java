package com.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME) //RUNTIME
@Target(ElementType.TYPE) //on TYPE only
public @interface TesterInfo {
	
	//must ignore this test ?
	public enum Priority {
		LOW, MEDIUM, HIGH
	}
	// Priority of this test
	Priority priority() default Priority.MEDIUM;
	
	String[] tags() default{""};
	
	//author of this test
	String createBy() default "rfruitet";
}
