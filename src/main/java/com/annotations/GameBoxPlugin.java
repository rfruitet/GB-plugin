package com.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface GameBoxPlugin {

	//author of this plugin
	String createBy() default "";

	//name of this plugin
	String pluginName() default "";

	//description of this plugin
	String description() default "";

	//type of this plugin
	public enum PluginType {
		GAME, TOOL
	}
	PluginType pluginType();

	//last modification of the plugin
	String lastModified() default "";
}
