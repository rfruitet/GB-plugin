package com.gameboxplugin.main;

import com.annotations.GameBoxPlugin;
import com.gameboxplugin.controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by rfruitet on 20/06/2016.
 */

@GameBoxPlugin(
        createBy = "rfruitet",
        pluginName = "GameName",
		description = "Description du jeu !",
        pluginType = GameBoxPlugin.PluginType.GAME,
        lastModified = "20/06/2016"
)

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
		
		Controller c = new Controller();

        Parent root = null;
		
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/plugin.fxml"));
            loader.setController(new Controller());
            root = loader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        primaryStage.setTitle("GameBoxPlugin - Hello World");
        primaryStage.setScene(new Scene(root, 600, 350));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}