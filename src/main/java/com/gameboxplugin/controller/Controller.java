package com.gameboxplugin.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Created by rfruitet on 20/06/2016.
 */
public class Controller {
	
    @FXML
	Label label;

    public void initialize() {
        label.setText("World!");
    }
}
